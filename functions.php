<?php
/**
 * Proper way to enqueue scripts and styles
 */
function wpdocs_theme_name_scripts() {
    wp_enqueue_style( 'style', get_template_directory_uri() . '/build/main.css' );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/build/main.js');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
